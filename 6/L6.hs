
-- import Control.Applicative

a1 :: Maybe Int
a1 = Just 1

a2 :: Maybe Int
a2 = Nothing

-- применить функцию к значению в контейнере

b1 = fmap show a1
b2 = fmap show a2

data Tree a = Leaf a
            | Branch (Tree a) (Tree a)
            deriving (Show)

instance Functor Tree where
  fmap f (Leaf a) = Leaf $ f a
  fmap f (Branch t1 t2) = Branch (fmap f t1) (fmap f t2)

data Fun r a = Fun (r -> a)

instance Functor (Fun r) where
  fmap f (Fun g) = Fun $ f . g

-- fmap (f . g) === fmap f . fmap g
-- fmap id === id

type Err = String
data Parser a = Parser
                { parse :: String ->
                  Either Err (a, String) }

instance Functor Parser where
  fmap f p = Parser $ \s -> case parse p s of
    Left err -> Left err
    Right (x, rest) -> Right (f x, rest)

instance Applicative Parser where
  pure a = Parser $ \s -> Right (a,s)
  (<*>) pf pa = Parser $ \s -> case parse pf s of
    Left err -> Left err
    Right (f,r1) -> case parse pa r1 of
      Left err -> Left err
      Right (x,r2) -> Right (f x, r2)

anyChar :: Parser Char
anyChar = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) -> Right (c, cs)

many :: Parser a -> Parser [a]
many p = Parser $ \s -> case parse p s of
  Left _ -> Right ([],s)
  Right (x,r1) -> case parse (many p) r1 of
    Left _ -> Right ([x],r1)
    Right (xs,r2) -> Right (x:xs, r2)

matching :: (Char -> Bool) -> Parser Char
matching p = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) | p c -> Right (c, cs)
         | otherwise -> Left $ "Char " ++ [c] ++
                        " is wrong"

digit :: Parser Char
digit = matching (\c -> '0'<=c && c<='9')

char :: Char -> Parser Char
char c = matching (==c)

number :: Parser Int
number = pure read <*> many digit
           -- read <$> many digit

(<|>) :: Parser a -> Parser a -> Parser a
(<|>) pa pb = Parser $ \s -> case parse pa s of
  Left err -> parse pb s
  Right a -> Right a

<<<<<<< HEAD
<<<<<<< HEAD
  word :: Parser String
  word = many $ matching (/=' ')

  words :: Parser [String]
  words = many $ pure (\a b -> a)
                 <*> word
                 <*> char ' '
=======
=======
>>>>>>> 38a55aa614f8363c12ab3ddf70799b10a912b126
word :: Parser String
word = many $ matching (/=' ')

words :: Parser [String]
words = many $ pure (\a b -> a)
               <*> word
               <*> char ' '
<<<<<<< HEAD
>>>>>>> 38a55aa614f8363c12ab3ddf70799b10a912b126
=======
>>>>>>> 38a55aa614f8363c12ab3ddf70799b10a912b126

data Term = Var Char
          | Lam Char Term
          | App Term Term
          deriving (Eq,Show)

term :: Parser Term
term = lam <|> (app <|> var)

lam =  (\_ v _ _ t -> Lam v t)
   <$> char '\\'
   <*> anyChar
   <*> char '-'
   <*> char '>'
   <*> term

app =  (\_ t1 _ t2 _ -> App t1 t2)
   <$> char '('
   <*> term
   <*> char ' '
   <*> term
   <*> char ')'

var = Var <$> matching (\c -> 'a'<=c && c<='z')

