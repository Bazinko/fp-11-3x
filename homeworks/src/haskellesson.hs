intlist2 :: [Int]
intlist2 = 5 : 4 : 3 : []

intlist3 :: [Int]
intlist3 = [5,4,3]

length' :: [a] -> Int
length' [] = 0
length' (x:xs) = 1 + length' xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' pred [] = []
filter' pred (a:as) | pred a = a : filter' pred as
					| otherwise = filter' pred as



(+++) :: [a] -> [a] -> [a]
[] +++ x2 = x2
(x1:x1s) +++ x2 = x1 : (x1s +++ x2)

map' :: (a -> b) -> [a] -> [b]
map' f [] = []
map' f (a:as) = b : bs

foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' f b [] = b
foldl' f b (a:as) = foldl' f (f b a) as


data Maybe' a = Nothing'
			  | Just' a

fromMaybe' :: a -> Maybe' a -> a
fromMaybe' def ma = case ma of
	Nothing' -> def
	Just' a -> a

fromMaybe'' :: a -> Maybe' a -> a
fromMaybe'' def Nothing' = def
fromMaybe'' _ (Just' a) = a

maybe' :: b -> (a -> b) -> Maybe' a -> b
maybe' b _ Nothing' = b
maybe' _ f (Just' a) = f a

catMaybes' :: [Maybe' a] -> [a]
catMaybes' [] = []

