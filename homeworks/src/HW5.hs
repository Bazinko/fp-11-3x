-- 2016-04-05 / 2016-04-12

module HW5
       ( Parser (..)
       , dyckLanguage
       , Arith (..)
       , arith
       , Optional (..)
       ) where

{--==========  PARSER ==========-}

type Err = String
data Parser a = Parser
                { parse :: String ->
                  Either Err (a, String) }

instance Functor Parser where
  fmap f p = Parser $ \s -> case parse p s of
    Left err -> Left err
    Right (x, rest) -> Right (f x, rest)

instance Applicative Parser where
  pure a = Parser $ \s -> Right (a,s)
  (<*>) pf pa = Parser $ \s -> case parse pf s of
    Left err -> Left err
    Right (f,r1) -> case parse pa r1 of
      Left err -> Left err
      Right (x,r2) -> Right (f x, r2)

anyChar :: Parser Char
anyChar = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) -> Right (c, cs)

many :: Parser a -> Parser [a]
many p = Parser $ \s -> case parse p s of
  Left _ -> Right ([],s)
  Right (x,r1) -> case parse (many p) r1 of
    Left _ -> Right ([x],r1)
    Right (xs,r2) -> Right (x:xs, r2)

matching :: (Char -> Bool) -> Parser Char
matching p = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) | p c -> Right (c, cs)
         | otherwise -> Left $ "Char " ++ [c] ++
                        " is wrong"

digit :: Parser Char
digit = matching (\c -> '0'<=c && c<='9')

char :: Char -> Parser Char
char c = matching (==c)

number :: Parser Int
number = pure read <*> many digit
           -- read <$> many digit

(<|>) :: Parser a -> Parser a -> Parser a
(<|>) pa pb = Parser $ \s -> case parse pa s of
  Left err -> parse pb s
  Right a -> Right a

{--========  END PARSER ========-}

-- ((())())
dyckLanguage :: Parser String
dyckLanguage = Parser $ \s -> case dyckTest s "" of
  False -> Left "Parsing error"
  True -> Right (s, "")

dyckTest :: String -> String -> Bool
dyckTest "" "" = True
dyckTest "" _ = False
dyckTest (head : tail) "" = dyckTest tail [head]
dyckTest (head : tail) (stackHead : stackTail)
  | head == '(' = dyckTest tail (head : stackHead : stackTail)
  | head == ')' && stackHead == '(' = dyckTest tail stackTail
  | head == ')' && stackHead /= '(' = False


data Arith = Plus Arith Arith
           | Minus Arith Arith
           | Mul Arith Arith
           | Number Int
           deriving (Eq,Show)

type Operands = [Arith]
type Operators  = [Char]

arithParse :: String -> Operands -> Operators -> Arith
arithParse "" operands [] = head operands

arithParse "" operands (curOper : operators)
  | isPlus curOper = arithParse "" ((Plus (operands!!1) (operands!!0)) : (drop 2 $ operands)) operators
  | isMinus curOper = arithParse "" ((Minus (operands!!1) (operands!!0)) : (drop 2 $ operands)) operators
  | isMul curOper = arithParse "" ((Mul (operands!!1) (operands!!0)) : (drop 2 $ operands)) operators

arithParse (curSym : tail) operands operators
  | isDigit curSym = arithParse (stripNumber $ tail) ((getNumber (curSym : tail)) : operands) operators
  | length operators == 0 = arithParse tail operands (curSym : operators)

  | ifAddPlus = arithParse tail ((Plus (operands!!1) (operands!!0)) : (drop 2 $ operands)) newOperators
  | ifAddMinus = arithParse tail ((Minus (operands!!1) (operands!!0)) : (drop 2 $ operands)) newOperators
  | ifAddMul = arithParse tail ((Mul (operands!!1) (operands!!0)) : (drop 2 $ operands)) newOperators

  | isMul curSym = arithParse tail operands (curSym : operators)

  where newOperators = curSym : (drop 1 $ operators)
        ifAddPlus = (isPlus curSym || isMinus curSym) && head operators == '+'
        ifAddMinus = (isPlus curSym || isMinus curSym) && head operators == '-'
        ifAddMul = (isPlus curSym || isMinus curSym || isMul curSym) && head operators == '*'

stripNumber :: String -> String
stripNumber str = case (parse $ number) str of
  Right (number, s) -> s

getNumber :: String -> Arith
getNumber str = case (parse $ number) str of
  Right (number, s) -> Number number

isDigit :: Char -> Bool
isDigit c = ((parse $ digit) $ [c]) == Right (c, "")

isPlus :: Char -> Bool
isPlus c = ((parse $ (char '+')) $ [c]) == Right (c, "")

isMinus :: Char -> Bool
isMinus c = ((parse $ (char '-')) $ [c]) == Right (c, "")

isMul :: Char -> Bool
isMul c = ((parse $ (char '*')) $ [c]) == Right (c, "")

-- ((123+4321)*(321-3123))+(123+321)
{- Add (Mul (Add (Const 123) (Const 4321))
            (Sub (Const 321) (Const 3213)))
       (Add (Const 123) (Const 321))
-}
arith :: Parser Arith
arith = Parser $ \s -> Right (arithParse s [] [], "")

-- Инстансы функтора и аппликативного функтора

data Optional a = NoParam
                | Param a
                deriving (Eq,Show)

instance Functor Optional where
fmap f opt = case opt of
	Param value -> Param $ (f value)
    _ -> NoParam

instance Applicative Optional where
  pure  a = Param a
  (<*>) pf pa = case pf of
  	NoParam -> NoParam
  	Param f -> case pa of
  		NoParam -> NoParam
  		Param value -> Param (f value)

