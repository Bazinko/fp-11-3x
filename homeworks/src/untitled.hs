data List a = Nil
			| Cons a (List a)
			deriving (Eq, Show)

instance Eq A where
	(A x) == B = True
	_ == _ = False

instance Eq a => Ord (List a) where
	compare Nil Nil = EQ
	compare Nil (Cons _ _) = LT
	compare (Cons _ _) Nil = GT
	compare (Cons a as) (Cons b bs) = case compare a b of
		LT -> LT
		EQ -> compare as bs
		GT -> GT